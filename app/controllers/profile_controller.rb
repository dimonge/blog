class ProfileController < ApplicationController

	def index
		@name = "My name is Peter Shodeinde. An enthusiastic software developer and entrepreneur who always find new ways to get things done. Born in Nigeria to a Christian family with a brother and a sister."
		@biography = "I have a BSc in Information Technology with major in Software Engineering from Savonia University of Applied Science. I have worked as a software developer with focus in web and mobile development and automation testing."
	end
  def show
  end
end
