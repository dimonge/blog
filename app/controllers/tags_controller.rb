class TagsController < ApplicationController
  before_action :find_tag, only: [:show, :destroy]
  #before_filter :require_login, only: [:destroy]

  def index
    @tag = Tag.all
  end
  
  def show
  end
  
  def destroy
    @post = Post.find(params[:id])
    @tag.destroy
    redirect_to post_path(@post)
  end
  
  private 
  def find_tag
    @tag = Tag.find(params[:id])
  end

end
